def gen_country_code():
    import string
    import itertools

    _data = set()
    for item in itertools.permutations(list(string.ascii_lowercase), r=2):
        _data.add(''.join(item))

    return _data

