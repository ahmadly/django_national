from django.db import models


class CityManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=self.model.CITY)


class ProvinceManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=self.model.PROVINCE)


class StateManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=self.model.STATE)


class CountryManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=self.model.COUNTRY)
