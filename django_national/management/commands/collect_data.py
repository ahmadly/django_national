import os
from django.core.management import BaseCommand
from core import settings
from ...utils import gen_country_code


class Command(BaseCommand):
    def gen_country_data(self, country_code: str):
        import requests
        from fake_useragent import UserAgent

        ua = UserAgent(verify_ssl=False)

        headers = {'User-Agent': ua.random}
        _base_url = 'https://simplemaps.com/static/data/country-cities'

        with requests.get(f'{_base_url}/{country_code}/{country_code}.csv', stream=True, headers=headers) as response:
            if response.ok:
                with open(os.path.join(settings.BASE_DIR, 'data', f'{country_code}.csv'), 'wb', ) as file:
                    file.write(response.content)
            elif response.status_code == 404:
                pass
            else:
                self.stdout.write(self.style.ERROR(response.status_code))

    def handle(self, *args, **options):
        import random
        import time

        for country in gen_country_code():
            self.gen_country_data(country)
            time.sleep(random.randint(3, 20))
