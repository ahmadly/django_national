from django.core.management import BaseCommand
import csv

from models import Region


class Command(BaseCommand):
    def handle(self, *args, **options):
        _data = set()
        with open('/Users/ahmad/PycharmProjects/WorldCitiesDatabase/all.csv', encoding='utf-8') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                if 'region' in row:
                    _data.add(row['region'].strip().lower())
        for i in _data:
            if i:
                Region.objects.create(
                    type=Region.CONTINENT,
                    name=i,
                )
