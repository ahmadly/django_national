from django.db import models
# from .manager import CityManager, ProvinceManager, StateManager, CountryManager


class Region(models.Model):
    """
    https://en.wikipedia.org/wiki/Geocoding
    https://www.iso.org/glossary-for-iso-3166.html
    https://en.wikipedia.org/wiki/List_of_country_calling_codes
    """
    CITY = 1
    PROVINCE = 2
    STATE = 3
    COUNTRY = 4
    CONTINENT = 5
    RegionTypeChoices = (
        (CITY, 'City'),
        (PROVINCE, 'Province'),
        (STATE, 'State'),
        (COUNTRY, 'Country'),
        (CONTINENT, 'Continent')
    )
    type = models.IntegerField(
        choices=RegionTypeChoices
    )
    name = models.CharField(
        max_length=100, unique=True, db_index=True
    )
    alpha2 = models.CharField(
        max_length=2, unique=True, db_index=True, blank=True, null=True,
        help_text='a two-letter code that represents a country name, recommended as the general purpose code')
    alpha3 = models.CharField(
        max_length=3, unique=True, db_index=True, blank=True, null=True,
        help_text='a three-letter code that represents a country name, which is usually more '
                  'closely related to the country name')
    alpha4 = models.CharField(
        max_length=4, unique=True, db_index=True, blank=True, null=True,
        help_text='a four-letter code that represents a country name that is no longer in use. '
                  'The structure depends on the reason why the country name was '
                  'removed from ISO 3166-1 and added to ISO 3166-3.')
    numeric_code = models.PositiveIntegerField(
        blank=True, null=True,
        help_text=('ISO 3166-1 numeric (or numeric-3) codes are three-digit country codes '
                   'defined in ISO 3166-1, part of the ISO 3166 standard published by the '
                   'International Organization for Standardization (ISO), to represent countries, '
                   'dependent territories, and special areas of geographical interest.'
                   ),
    )

    subdivision_code = models.CharField(
        blank=True, null=True, max_length=10,
        help_text=('ISO 3166-2 code that represents the name of a principal subdivision (e.g province or state) '
                   'of countries coded in ISO 3166-1. This code is based on the two-letter code '
                   'element from ISO 3166-1 followed by a separator and up to three alphanumeric '
                   'characters. The characters after the separator cannot be used on their own to '
                   'denote a subdivision, they must be preceded by the alpha-2 country code.')
    )
    latitude = models.FloatField(
        blank=True, null=True,

    )

    longitude = models.FloatField(
        blank=True, null=True,

    )

    # region = models.CharField()
    # sub_region = models.CharField()
    calling_code = models.CharField(
        max_length=10, unique=True, db_index=True, blank=True, null=True
    )
    # pronunciation = models.CharField(max_length=100, unique=True, db_index=True, blank=True, null=True)
    # currency = models.CharField(max_length=3, blank=True, null=True)
    parent = models.ForeignKey('self', blank=True, null=True)
    # language = models.CharField(choices=settings.LANGUAGES)

    objects = models.Manager()
    # cities = CityManager()
    # provinces = ProvinceManager()
    # states = StateManager()
    # countries = CountryManager()

    def __str__(self):
        return '{id}: {name} ({type})'.format(id=self.id, name=self.name, type=self.get_type_display())

    @property
    def children(self):
        return Region.objects.filter(parent=self.id)

    def is_city(self):
        return self.type == self.CITY
